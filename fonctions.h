/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 2                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :  IP                                                             *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :     Mounie-robin                                             *
*                                                                             *
*  Nom-prénom2 :     GarciaPonce-cristiant                                    *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :    fonction.h                                             *
*                                                                             *
******************************************************************************/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "regex.h"

void extraireLesChamps(char chaine[19],char tab[5][4]);
void ConvertirNum(char tab[5][4],int t[5]);
void DecoderIp(int t[5],char * classe,bool * a);
int estIpValable (char chaineParam[]);
void affichage(char classe, bool visibilite);
