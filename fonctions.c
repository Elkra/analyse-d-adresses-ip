/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 2                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :  IP                                                             *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 :     Mounie-robin                                             *
*                                                                             *
*  Nom-prénom2 :     GarciaPonce-cristiant                                    *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :    fonction.c                                             *
*                                                                             *
******************************************************************************/
#include "fonctions.h"

void extraireLesChamps(char chaine[19],char tab[5][4])
{
    char str[4];
    int dp = 0;
    int nb = 0;
    for (int i = 0; i< 19; i++)
    {
        if (chaine[i] == '.'||chaine[i] == '/'){
            strncpy(tab[nb],chaine + dp,(i-dp));
            dp = i+1;
            nb = nb + 1;
        }
        if (chaine[i] == '/'){
            strncpy(tab[4],chaine + i + 1 ,2);
        }
    }
}

void ConvertirNum(char tab[5][4],int t[5])
{
    for (int i = 0; i< 5; i++)
    {
        t[i]=strtol(tab[i],NULL, 10);
    }
}  

void DecoderIp(int t[5],char * classe,bool * a){
    if (t[0] <= 127){
        *classe = 'A';
    }
    if (t[0] <= 191 && t[0] >= 128){
        *classe = 'B';
    }
    if (t[0] <= 223 && t[0] >= 192){
        *classe = 'C';
    }
    if (t[0] >= 240){
        *classe = 'D';
    }
    *a = false;
    if( t[0] == 10) {
        *a = true;
    }
        if( (t[0] == 172) && (t[1] <= 31) && (t[1] >= 16)) {
        *a = true;
    }
        if((t[0] == 192)&&(t[1] == 168)) {
        *a = true;
        
    }
}

int estIpValable (char chaineParam[]){
    regex_t adresseType;
    int estValide = regcomp(&adresseType,"25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9]?)){3}\\/[[:digit:]]{1,2}",REG_EXTENDED|REG_NOSUB);
    
        if (0==estValide){
            estValide = regexec (&adresseType, chaineParam,0,NULL,0);
            return estValide;
        }else{
            return -1;
        }
}

void affichage(char classe, bool visibilite){
    printf("La classe de l'ip est %c sa visibilité est : ", classe);
    FILE* fic;
    fic = fopen("Resultat.txt","w");
    if (fic == NULL) {
    printf("Erreur lors de l'ouverture du fichier");
    }
    fprintf(fic,"La classe de l'ip est %c sa visibilité est : ",classe);
    if (visibilite ==0){
        printf("publique\n");
        fprintf(fic,"publique\n",classe);
    }else{
        printf("privée\n");
        fprintf(fic,"privée\n",classe);
    }
    fclose(fic);
}
